## A propos du projet

Le projet a été fait en Laravel par Jacques CAI et Thomas CHATEL.
Il y a 18 tests unitaires pour vérifier le comportement des 3 models.

## Run les tests

Pour run les tests, il faut executer la commande suivante:
```
php artisan test
```
