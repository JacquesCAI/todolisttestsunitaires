<?php

namespace App\Models;

use App\Services\ToDoListItemService;
use App\Services\MailService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

class ToDoList extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function owner()
    {
        return $this->morphOne(User::class, 'owner');
    }

    public function items()
    {
        return $this->hasMany(ToDoListItems::class);
    }

    public function isValid()
    {
        if (empty($this->name)) {
            return new Exception('Votre toDoList doit avoir un nom');
        }
        return true;
    }

    public function canAddItems()
    {
        $nbItems = $this->getItemsNumber();
        if ($nbItems >= 10) {
            return new Exception('Vous avez déjà 10 items sur votre toDoList');
        }
        //$this->items()[count($this->items()) - 1]->created_at
        if (
            $this->getLastItemCreatedAt() >
            date('Y/m/d H:i:s', strtotime('-30 minutes'))
        ) {
            return new Exception(
                'Le dernier item a été inséré il y a moins de 30 minutees'
            );
        }
        if ($nbItems = 7) {
            $mailService = new MailService();
            if (!$mailService->sendAlertMail()) {
                return new Exception('Le mail n\'a pas pu être envoyé');
            }
        }
        return true;
    }

    public function getItemsNumber()
    {
        return count($this->items());
    }

    public function getLastItemCreatedAt()
    {
        return $this->items()[count($this->items()) - 1]->created_at;
    }
}
