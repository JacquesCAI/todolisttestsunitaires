<?php

namespace App\Models;

use App\Services\ToDoListItemService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

class ToDoListItems extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'content',
        'created_at'
    ];

    public function toDoList()
    {
        return $this->belongsTo(ToDoList::class);
    }

    public function isValid(ToDoListItemService $toDoListItemService) {
        if (empty($this->name))
            return new Exception('L\'item doit avoir un nom');
        if (!$toDoListItemService->isUniqueName($this->name))
            return new Exception('Nom déjà pris');
        if (strlen($this->content) > 1000)
            return new Exception('Le contenu de l\'item est trop long');

        return true;
    }
}
