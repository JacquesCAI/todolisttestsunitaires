<?php

namespace App\Services;

use App\Models\ToDoListItems;

class ToDoListItemService {

    public function isUniqueName(string $name) {
        $result = ToDoListItems::where('name', $name)->get();
        if (!empty($result))
            return false;
        return true;
    }

}
