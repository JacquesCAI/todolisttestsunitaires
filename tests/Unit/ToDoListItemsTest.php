<?php

namespace Tests\Unit;

use App\Models\ToDoListItems;
use App\Services\ToDoListItemService;
use PHPUnit\Framework\TestCase;

class ToDoListItemsTest extends TestCase
{

    private $toDoListItem;
    private $toDoListItemServiceMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->toDoListItem = new ToDoListItems([
            'name' => 'Mon Item',
            'content' => 'Contenu de l\'item'
        ]);

        $this->toDoListItemServiceMock = $this->getMockBuilder(ToDoListItemService::class)->onlyMethods(['isUniqueName'])->getMock();
        $this->toDoListItemServiceMock->expects($this->any())->method('isUniqueName')->willReturn(true);
    }

    public function testToDoListItemIsValid() {
        $this->assertTrue($this->toDoListItem->isValid($this->toDoListItemServiceMock));
    }

    public function testToDoListItemWithEmptyName()
    {
        $this->toDoListItem->name = '';
        $exception = $this->toDoListItem->isValid($this->toDoListItemServiceMock);
        $this->assertEquals('L\'item doit avoir un nom', $exception->getMessage());
    }

    public function testToDoListItemWithExistingName()
    {
        $toDoListItemServiceMock = $this->getMockBuilder(ToDoListItemService::class)->onlyMethods(['isUniqueName'])->getMock();
        $toDoListItemServiceMock->expects($this->once())->method('isUniqueName')->willReturn(false);
        $exception = $this->toDoListItem->isValid($toDoListItemServiceMock);
        $this->assertEquals('Nom déjà pris', $exception->getMessage());
    }

    public function testToDoListItemWithOver1000Charactère()
    {
        $this->toDoListItem->content = '8YaQPNN0AHksbbNVsw2Hks8mFnIU8f1q4DV6aCxXbQHLlcC5BCuyJwbISwXlBBARoSOviftIWWGoZiLFR5rLex8TzQbIcMK0wPzlDzvFv9RolEtct9CYazqv1DIf7yQxTgUDvrmkFbCRuGe0VNk2HRjLVusgWAKdTlMJ9GFQXpS9Iih44AFB5ZiNMKBCOOuflCh3m1we35qFiqecn3tjZhJKvjvXQ1qSqA3StKTVwdn0NUmoLp863wVTdo9NqPJS027Y1hAPG9A8aslbAT8ksa7NmwL8KiyTY67OmtXMYUKf1WqyeidXUECeI5gU5ykeChJYSVeUUVoDRKQgW6wpHOTavPZf8qEeVwc9qlNhXvYckkecGG8x8FkijnaMnDdL2fKOzea4WFuHADGaiMEicQrYaacXKQoOHvlZJC8qNjCqcrt04sjBRzU6eQySkAlGax0Yi16dyXjjDV8S7JpLTSVYcOK6ECF6IKK8LMeVVIqTDAOAkePSg81nHzRaP4BZOsJvHh0IbYD5NhwGAxZKmf02rFRVmpsywmq647zQ7NpSqkAuIgkdjs68UlQhpE1Gs7KwlCx80D2QZdZUyx83gU9Z2lR8wCKRFMyuSQOtlcfcZCU9nN4vtfLisxOmQr5Gu7LQgEcnpNsy7IuCryJwI88k6v4VlEAs3N3etSS7TyXZh6HuE9LkIl5UU36tN0qh0qdxItRLaanGE2S2XjHQlkztnjTUDXJdIh9REDvp3XT2ZKHRR8h9LywuNMTPHjS0SBImc86B4G2zRZm4oezbHwz336ASO4DAiFHZAHlzSWpabiWozf82fMJ7AMy4PUpPNYGpWDRlF1tnQmWQOeqPQQ6QnhEoyihP2of5qCu3WY0dERMxP06nCFuvof6rwX3q0bW8kmEUQDvcb0JDQ5EoM4Xr4oqfm0cprZ3c2A3mU1IyrwwJ0bgcgB7BR6Jspv2bQbseLiMCrVY3S4oqGqnAKL5Y48fnsB8K8BN26cFNI1UvB';
        $exception = $this->toDoListItem->isValid($this->toDoListItemServiceMock);
        $this->assertEquals('Le contenu de l\'item est trop long', $exception->getMessage());
    }
}
