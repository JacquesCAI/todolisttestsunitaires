<?php

namespace Tests\Unit;

use App\Models\ToDoList;
use App\Models\ToDoListItems;
use App\Models\User;
use App\Services\ToDoListItemService;
use PHPUnit\Framework\TestCase;

class ToDoListTest extends TestCase
{

    private $user;
    private $toDoList;

    private $toDoListMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = new User([
            'firstname' => 'Thomas',
            'lastname' => 'Chatel',
            'email' => 'dev@test.com',
            'password' => 'testtest',
            'age' => 22
        ]);

        $this->toDoList = new ToDoList([
            'name' => 'Ma toDoList'
        ]);

        $this->toDoListMock = $this->getMockBuilder(ToDoList::class)->onlyMethods(['getItemsNumber','getLastItemCreatedAt'])->getMock();
    }

    public function testToDoListIsValid()
    {
        $this->assertTrue($this->toDoList->isValid());
    }

    public function testToDoListWithEmptyName()
    {
        $this->toDoList->name = '';
        $exception = $this->toDoList->isValid();
        $this->assertEquals('Votre toDoList doit avoir un nom', $exception->getMessage());
    }

    public function testToDoListInsertItemBefore30minutes()
    {
        $this->toDoListMock->expects($this->once())->method('getLastItemCreatedAt')->willReturn(date("Y/m/d H:i:s", strtotime("-20 minutes")));
        $exception = $this->toDoListMock->canAddItems();
        $this->assertEquals('Le dernier item a été inséré il y a moins de 30 minutees', $exception->getMessage());
    }

    public function testToDoListInsert8thItem()
    {
        $this->toDoListMock->expects($this->once())->method('getItemsNumber')->willReturn(7);
        $this->assertTrue($this->toDoListMock->canAddItems());
    }

    public function testToDoListInsert11thItem()
    {
        $this->toDoListMock->expects($this->once())->method('getItemsNumber')->willReturn(10);
        $exception = $this->toDoListMock->canAddItems();
        $this->assertEquals('Vous avez déjà 10 items sur votre toDoList', $exception->getMessage());
    }

}
