<?php

namespace Tests\Unit;

use App\Models\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;
    private $userMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User([
            'firstname' => 'Thomas',
            'lastname' => 'Chatel',
            'email' => 'dev@test.com',
            'password' => 'testtest',
            'age' => 22
        ]);

        /**
         * $this->getMockBuilder(User::class) = new User()
         * only
         */
        $this->userMock = $this->getMockBuilder(User::class)->onlyMethods(['hasToDoList'])->getMock();

    }

    public function testValidMethod()
    {
        $this->assertTrue($this->user->isValid());
    }

    public function testUserWithNoFirstname()
    {
        $this->user->firstname = '';
        $exception = $this->user->isValid();
        $this->assertEquals('Le prénom ou le nom n\'est pas renseigné', $exception->getMessage());
    }

    public function testUserWithNoLastname()
    {
        $this->user->lastname = '';
        $exception = $this->user->isValid();
        $this->assertEquals('Le prénom ou le nom n\'est pas renseigné', $exception->getMessage());
    }

    public function testUserWithLess8Characters()
    {
        $this->user->password = 'test';
        $exception = $this->user->isValid();
        $this->assertEquals('Le mot de passe doit faire entre 8 et 40 caractères', $exception->getMessage());
    }

    public function testUserWithOver40Characters()
    {
        $this->user->password = 'Pydj4LKXzTbzN4BKntAhKU1uzJ1JIFFP2VbEzgQTOa';
        $exception = $this->user->isValid();
        $this->assertEquals('Le mot de passe doit faire entre 8 et 40 caractères', $exception->getMessage());
    }

    public function testUserWithInvalidEmailFormat()
    {
        $this->user->email = 'testdev';
        $exception = $this->user->isValid();
        $this->assertEquals('Email non valide', $exception->getMessage());
    }

    public function testUserWithLessThan13YearsOld()
    {
        $this->user->age = 11;
        $exception = $this->user->isValid();
        $this->assertEquals('L\'utilisateur doit au moins avoir 13ans', $exception->getMessage());
    }

    public function testUserWithNegativeAge()
    {
        $this->user->age = -10;
        $exception = $this->user->isValid();
        $this->assertEquals('L\'utilisateur doit au moins avoir 13ans', $exception->getMessage());
    }

    public function testUserCanAddSecondToDoList()
    {
        $this->userMock->expects($this->once())->method('hasToDoList')->willReturn(true);
        $exception = $this->userMock->canCreateToDoList();
        $this->assertEquals('Vous avez déjà une toDoList', $exception->getMessage());
    }
}
